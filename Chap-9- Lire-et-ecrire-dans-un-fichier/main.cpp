#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
    // |=============================================| //
    // |----------> Lire et ecrire dans un fichier <-| //
    // |=============================================| //


    //-------------------------//
    //--> Ecrire dans un Fichier
    //-------------------------//
    string const nomFichier("log/scores.txt");

    ofstream monFlux(nomFichier.c_str(), ios::app); // D�claration d'un flux permettant d'ecrire dans le fichier Scores.txt


    if (monFlux) // Test sil'ouverture du fichier c'est bien passer
    {
        // Tout et OK, on peut utiliser le fichier
        std::cout << "Quel est ton username :" << std::endl;
        string name;
        std::cin >> name;

        std::cout << "Et tu a quel age ?" << std::endl;
        int age;
        std::cin >> age;

        monFlux << "USERNAME => " << name << " || AGE => " << age << " ans" << std::endl;
    }
    else
    {
        std::cout << "ERREUR : Impossible d'ouvrir le fichier !!! " << std::endl;
    }



    //-------------------------//
    //--> Lire un Fichier
    //-------------------------//

    ifstream myFlux("log/data.txt"); // Ouverture d'un fichier en lecture

    if (myFlux) // Tout est pret pour la lecture
    {
        std::cout << "=============================================" << endl;
        std::cout << "------------ LECTURE LIGNE PAR LIGNE --------" << endl;
        std::cout << "=============================================" << endl;
        string ligne;
        getline(myFlux, ligne);

        std::cout << "La ligne contient ceci : " << ligne << std::endl;


        std::cout << "==========================================" << endl;
        std::cout << "------------ LECTURE MOT PAR MOT ---------" << endl;
        std::cout << "==========================================" << endl;
        // Exemple pour lire des doubles (nombres � virgules)
        //double nombre;
        //myFlux >> nombre;

        string mot;
        myFlux >> mot; // Lit un mot depuis le fichier

        std::cout << "le mot est : " << mot << std::endl;


        // INPORTANT On change le mode//
        myFlux.ignore();

        std::cout << "====================================================" << endl;
        std::cout << "------------ LECTURE CARACTERE PAR CARACTERE--------" << endl;
        std::cout << "====================================================" << endl;
        char a,b,c,d,e,f;
        myFlux.get(a);
        myFlux.get(b);
        myFlux.get(c);
        myFlux.get(d);
        myFlux.get(e);
        myFlux.get(f);
        std::cout << "Les lettres lu sont : " << a << b << c << d << e << f << std::endl;


    }
    else
    {
        std::cout << "ERREUR : Impossible d'ouvrir le fichier en lecture !!! " << std::endl;
    }


    myFlux.close();

    //#################################################################"//

    //===========================
    // LIRE UN FICHIER EN ENTIER
    //===========================

    std::cout << "==========================================" << endl;
    std::cout << "------------ LECTURE D'UN FICHIER --------" << endl;
    std::cout << "==========================================" << endl;


    ifstream fichier("log/fichier.txt");

    if (fichier)
    {
        // Ouverture OK, n peut donc lire le fichier

        string line; // Variable pour stockerles lignes lu

        // Tant qu'on est pas a la fin du fichier on lie
        while (getline(fichier, line))
        {
            std::cout << line << endl;
        }
    }

    //fichier.close();


    //================================================
    // CONNAITRE LA POSITION DU CURSEUR DANS UN FICHIER
    //================================================

    // tellg() ==> ifstream (lecture) || tellp() ==> ofstream (ecriture)
    //ofstream myFile("log/fichier.txt");
    int position = fichier.tellg(); //On r�up�re la position

    std::cout << "Nous nous situons au " << position <<  "ieme caractere du fichier." << endl;

    //======================================
    // DEPLACER LE CURSEUR DANS UN FICHIER
    //======================================

    // seekg() ==> ifstream(lecture) || seekp() ==> ofstream(ecriture)
    // fichier.seekg(nbCaracteres, position);
    fichier.seekg(10,ios::beg);

    std::cout << "Apr�s deplacement, nous nous situons au " << position <<  "ieme caractere du fichier." << endl;


    //======================================
    // CONNAITRE LA TAILLE D'UN FICHIER
    //======================================
    ifstream file2("log/scores.txt"); // Ouverture fichier en lecture
    file2.seekg(0, ios::end); //On se deplace � la fin du fichier

    int taille;
    taille = file2.tellg();
    // On recup�re la position qui correspond donc  a la taille du fichier (nombre de caracteres)

    std::cout << "Taille du fichier : " << taille << " octets" << endl;
    //---FIN FU PROG----//
    return 0;
}


