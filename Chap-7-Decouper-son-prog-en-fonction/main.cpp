#include <iostream>
#include "math.h"

using namespace std;

// Fonction sans argument
string demanderNom()
{
    std::cout << "Entrez votre nom : ";
    std::string nom;
    std::cin >> nom;

    return nom;
}


// Fonction qui ne renvoit rien
void direBonjour()
{
    std::cout << "Bonjour !" << endl; //Comme rien ne ressort, il n'y a pas de return !
}





int main()
{
    int a(2),b(2);
    cout << "Valeur de a : " << a << endl;
    cout << "Valeur de b : " << b << endl;
    b = ajouteDeux(b); //Appel de la fonction
    cout << "Valeur de a : " << a << endl;
    cout << "Valeur de b : " << b << endl;


    direBonjour(); // Comme la fonction ne renvoie rien
    // On l'appele snas mettre la valeur de retour dans une variable

    // Calcul du carre d'un nmbre
    double nb, carreNombre;
    std::cout << "Entrez un nombre :";
    std::cin >> nb;

    carreNombre = carre(nb); // On appel la fonction Carre()

    std::cout << "Le carre de " << nb << " est " << carreNombre << endl;


    // Calcul du carre de tous les nmbre entre 1 et 20
    for (int i(1); i <= 20; i++) {
        std::cout << "Le carre de " << i << " est : " << carre(i) << endl;
    }

    // Fonction a deux argument
    int largeur, hauteur;
    std::cout << "Largeur du rectangle : ";
    std::cin >> largeur;
    std::cout << "Hauteur du rectangle : ";
    std::cin >> hauteur;

    dessineRectangle(largeur, hauteur);

    return 0;
}
