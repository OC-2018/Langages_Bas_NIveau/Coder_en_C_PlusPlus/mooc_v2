#ifndef MATH_H_INCLUDED
#define MATH_H_INCLUDED

int ajouteDeux(int nbRecu);

int addition(int a, int b);

int multiplication(double a, double b, double c);

double carre(double x);

void dessineRectangle(int l, int h);

#endif // MATH_H_INCLUDED
