#include <iostream>
#include <vector>

using namespace std;

int main()
{
    // EXMPLE D'UTILISATION: TABLEAU DE SCORE
    string joueur1("Nannoc");
    string joueur2("Joel");
    string joueur3("Mat21");
    string joueur4("Archimede");
    string joueur5("Isac Newton");

    int score1(118218);
    int score2(100432);
    int score3(87347);
    int score4(64523);
    int score5(31415);

    // Affichage de l'ensemble
    std::cout << "1) " << joueur1 << " " << score1 << endl;
    std::cout << "2) " << joueur2 << " " << score2 << endl;
    std::cout << "3) " << joueur3 << " " << score3 << endl;
    std::cout << "4) " << joueur4 << " " << score4 << endl;
    std::cout << "5) " << joueur5 << " " << score5 << endl;



    //===================================
    // DECLARER UN TABLEAU STATIQUE
    //==================================
    int const tailleTableau(5);
    int meilleurScores[tailleTableau];

    //----------------------------------
    // Acc�der au �l�ment d'un tableau
    //----------------------------------
    meilleurScores[0] = 118218;
    meilleurScores[1] = 100432;
    meilleurScores[2] = 87347;
    meilleurScores[3] = 64523;
    meilleurScores[4] = 31415;

    //----------------------
    // Parcourir un tableau
    //---------------------
    for (int i(0); i < tailleTableau; ++i )
    {
        std::cout << meilleurScores[i] << endl;
    }


    //===========================================
    // CAS CONCRET: Calcul de la moyennede l'ann�e
    //===========================================
    int const nbNotes(6);
    double notes[nbNotes];

    notes[0] = 12.5;
    notes[1] = 19.5;
    notes[2] = 6.5;
    notes[3] = 12;
    notes[4] = 14.5;
    notes[5] = 15;

    double moyenne;
    for (int i(0); i < nbNotes; i++)
    {
        moyenne += notes[i]; // On additionne toutes les notes
    }

    // EN arrivant ici, la variable "moyenne" contient la somme de toutes les notes
    // Il ne nous rete plus qu'a la diviser par le nmbre de note
    moyenne /= nbNotes;

    std::cout << "Votre moyenne est de : " << moyenne << endl;

    //============================================
    // Les tableaux et les fonctions
    //============================================
    //double calcul(double tableau[], int tailleTableau)
    //{
       //double tableau(0);
      // for(int i(0); i<tailleTableau; ++i) {
       //   tableau += tableau[i];
     //  }

   //    tableau /= tailleTableau;
 //      return tableau;
    //}



    // FIN du PROGRAMME
    return 0;
}
