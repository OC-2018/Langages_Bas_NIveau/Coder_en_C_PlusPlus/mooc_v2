#include <iostream>

using namespace std;

int main()
{
    //----------------------------------
	// Demander des infos a l'utilsateur
	//----------------------------------

	std::cout << "quel age avez-vous ? " << endl;
	int ageUser(0);      // On pr�pare un case m�moir epour stocker un entier

	std::cin >> ageUser; // On fait entrer un nombre par le user dans cette case
	std::cout << "Vous avez " << ageUser << " ans ! " << endl;

    std::cin.ignore();

	// PRENOM
	std::cout << "Quel est votre prenom ? " << endl;
	string nomUser("Sans nom"); // On initialise la var prenom avec un type string
	getline(std::cin, nomUser); // L'utilisateur saisie et enregistre son prenom

	// PI
	std::cout << "Combien vaut pi ? " << endl;
	double piUser(-1);  // On initialise la var pi avec un type double
	std::cin >> piUser; // L'utilisateur saisie et enregistre la r�ponse du user

	std::cout << "Vous vous appellez " << nomUser << " et vous pensez que PI vaut " << piUser << endl;


	//----------------------
	// MODIFIER DES VARIABLE
	//----------------------
	int unNombre(0);
	unNombre = 5;

	int a(4), b(5);


	std::cout << "A vaut : " << a << " et B vaut : " << b << endl;

	std::cout << "Affectation !" << endl;
	a = b;  // Affecte la valeur de B � A

	std::cout << "A vaut : " << a << " et B vaut : " << b << endl;


	//----------------
	// LES CONSTANTES
	//-----------------
	int const nbNiveau(10);
	string const password("wast");
	double const pi(3.14);
	unsigned int const pointsDeVieMax(100);


}
