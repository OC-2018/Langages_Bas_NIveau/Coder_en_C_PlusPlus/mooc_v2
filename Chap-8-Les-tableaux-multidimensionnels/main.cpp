#include <iostream>
#include "string"
#include <vector>

using namespace std;

int main()
{
    // -------------------------------------------
    // --> Declarer un tableau multi-dimensionnels
    // -------------------------------------------

    int const tailleX(5);
    int const tailleY(4);

    int tableau[tailleX][tailleY];


    // -------------------------------------------------------
    // --> Acc�der aus �l�ments dun tableau multi-dimensionnel
    // --------------------------------------------------------

    std::cout << "Le tableau est egal a : " << tableau << endl; // La valeur retourn� est hexadecimal


    // ------------------------------------
    // -> Creer des tableaux multidimensionnel de taille variable
    // ------------------------------------

    // Grille 2D d'entiers
    vector<vector<int> > grille;

    grille.push_back(vector<int>(5)); // On ajoute une ligne de 5 cases � notre grille
    grille.push_back(vector<int>(3,4)); // On ajoute une ligne de 3 cases contenant chacune le chiffre 4 � notre grille

    grille[0].push_back(8);     //Ajoute une case contenant 8 � la premi�re ligne du tableau

    grille[2][3] = 35; // On change la valeur de la cellule (2,3) de la grille

    int a(grille[2][3]);
    cout << a - 2;
    cout << "test";



    // FIN DU PROG
    return 0;
}
