#include <iostream> // Inclut la biblioth�que IoStream (affichage de texte)
#include <string> // Utilise des string


using namespace std; // Indique qul espace de nom on va utiliser

/*
Fonction principale "main"
Tousles programme commence par la fonction main
*/
int main()
{
	// AFFICHE DU TEXTE DAN LA CONSOLE //
	std::cout << "J'apprend le C++ !" << endl;

	// Il faut �chapper les caract�re sp�ciaux
	std::cout << "\"" << endl;
	std::cout << "\\" << endl;


	// ############################################### //
	// ######### CHAPITRE 4 - Utiliser la m�moire #### //
	// ############################################### //

	// DECLARER UNE VARIABLE
	int ageUser(16);
	int nbFriends(432);
	double pi(3.14159);
	bool isFriend(true);
	char letter('a');
	string userName("Joel Delzongle");

	// DECLARATION DE VARIABLES MULTIPLE
	int a(2), b(4), c(-12);
	string firstName("Lucas"), lastName("Heuriet");

    // DECLARRATION DE VARIABLE SANS INITIALISATION
    string nameUserOne;
    int nbPlayers;
    bool aGagner;

    // AFFICHER LA VALEUR D'UNE VARIABLE
    int years(32);
    std::cout << "Votre age est : ";
    std::cout << years << " ans" << endl;


    // LES REFERENCES
    int age(35); // D�claration de la variable
    int& maReference(age); // D�claration d'une r�f�rence nomm� maVar qui est accroch� � la variable age

    std::cout << "Vous avez " << age << " ans. (via variable)" << endl;
    std::cout << "Vous avez " << maReference << " ans. (via reference)" << endl;

    // FIN DU PROGRAMME
	return 0; // Termine la fonction main et donc le programme
}

