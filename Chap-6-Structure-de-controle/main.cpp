#include <iostream>

using namespace std;

int main()
{
    //=============
    // IF / ELSE
    //===========
    int nbEnfants(0);

    if (nbEnfants > 0) {
        std::cout << "Vous avez des enfants, bravo !" << endl;
    }
    else {
        std::cout << "Eh bien alors, vous n'avez pas d'enfant ?" << endl;
    }

    std::cout << "Fin du programme" << endl;


    //=====================
    // IF / ELSE IF / ELSE
    //====================
    if (nbEnfants == 0) {
        cout << "Eh bien alors, vous n'avez pas d'enfant ?" << endl;
    }
    else if (nbEnfants == 1) {
        cout << "Alors, c'est pour quand le deuxieme ?" << endl;
    }
    else if (nbEnfants == 2) {
        cout << "Quels beaux enfants vous avez la !" << endl;
    }
    else {
        cout << "Bon, il faut arreter de faire des gosses maintenant !" << endl;
    }


    //=========
    // SWITCH
    //========
    switch (nbEnfants) {
        case 0:
            cout << "Eh bien alors, vous n'avez pas d'enfant ?" << endl;
            break;

        case 1:
            cout << "Alors, c'est pour quand le deuxieme ?" << endl;
            break;

        case 2:
            cout << "Quels beaux enfants vous avez la !" << endl;
            break;

        default:
            cout << "Bon, il faut arreter de faire des gosses maintenant !" << endl;
            break;
    }


    //======================================
    // Booleen et combinaison de condition
    //=====================================
    bool adulte(true);

    if (adulte == true) {
        cout << "Vous etes un adulte !" << endl;
    }

    // Est �quivalent � (omission de la partie "== true"):
    if (adulte) {
        cout << "Vous etes un adulte !" << endl;
    }

    // Symbole ET
    if (adulte && nbEnfants >= 1)

    // Symbole OU
    if (nbEnfants == 1 || nbEnfants == 2)

    // NON
    if (!adulte)


    //=============
    // LES BOUCLES
    //=============

    //------
    // WHILE
    //-------
    nbEnfants = -1; // Nombre n�gatif pour pouvoir entrer dans la boucle

    while (nbEnfants < 0) {
        cout << "Combien d'enfants avez-vous ?" << endl;
        cin >> nbEnfants;
    }

    cout << "Merci d'avoir indique un nombre d'enfants correct. Vous en avez " << nbEnfants << endl;


    //---------
    // DO WHILE
    //---------
    nbEnfants = 0;

    do {
        cout << "Combien d'enfants avez-vous ?" << endl;
        cin >> nbEnfants;
    } while (nbEnfants < 0);

    cout << "Merci d'avoir indique un nombre d'enfants correct. Vous en avez " << nbEnfants << endl;


    //------------
    // FOR
    //------------
    //int compteur(0); // Initialise la var directement dans la boucle FOR
    for (int compteur = 0; compteur < 10; compteur++) {
        std::cout << compteur << endl;
    }

    return 0;
}
