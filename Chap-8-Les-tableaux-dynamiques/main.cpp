#include <iostream>
#include <vector>


using namespace std;

int main()
{


    //============================================
    // Les tableaux dynamique
    //============================================
    int const nbMeilleursScore(50); // Taille du tableau

    vector<int> score(nbMeilleursScore); // D�claration du tableau

    score[0] = 185652; // Remplissage de la premi�re case
    score[1] = 145234; // Remplissage de la deuxi�me case
    score[2] = 123224; // Remplisasge de la troisi�mre case
    score[3] = 85324; // Remplisasge de la quatri�emecase
    score[4] = 45322; // Remplisasge de la cinquieme  case

    //============================
    // Changer la taille du tableau
    //=============================

    // AJouter un element du tableau
    //--------------------------
    vector<int> tab(3,2); // Un tableau de 3 entiers valant tous 2
    tab.push_back(8); // On ajoute une case n�4 qui contient 8
    tab.push_back(18);
    tab.push_back(36);

    // TAB = [3,2,8,18,36]

    // SUpprimer le dernier element du tableau
    //----------------------------------------
    tab.pop_back();
    // TAB = [3,2,8,18]
    tab.pop_back();
    // TAB = [3,2,8]

    // REcuperer la taille d'un tableau
    //----------------------------------

    int tailleTab;
    tailleTab = tab.size();

    std::cout << "Le tableau contient : " << tailleTab << " elements !" << endl;


    return 0;
}
s
